<!--
 * @Author: 周凯
 * @Date: 2020-10-20 10:38:15
 * @LastEditTime: 2020-10-21 08:55:39
-->
- 运行 `npm install node-dahua-api-zhoukai`
- demo `https://gitee.com/EightDoor/dahua-api-demo`
- api 请求方式和node-dahua-api 是相同的


## 感谢
- https://github.com/nayrnet/node-dahua-api
